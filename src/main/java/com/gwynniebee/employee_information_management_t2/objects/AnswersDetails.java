/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

import java.sql.Date;

/**
 * @author yavvan
 */

public class AnswersDetails {

    private int answerId;
    private String ansBy, answer;
    private Date anstime;

    /**
     * @return answerId answerid
     */
    public int getanswerID() {
        return this.answerId;
    }

    /**
     * @param answerId answerid
     */
    public void setanswerID(int answerId) {
        this.answerId = answerId;
    }

    /**
     * @return answer answer
     */
    public String getanswer() {
        return this.answer;
    }

    /**
     * @param answer answer
     */
    public void setanswer(String answer) {
        this.answer = answer;
    }

    /**
     * @return ansBy ansBy
     */
    public String getansBy() {
        return this.ansBy;
    }

    /**
     * @param ansBy user
     */
    public void setansBy(String ansBy) {
        this.ansBy = ansBy;
    }

    /**
     * @return date date
     */
    public Date getanstime() {
        return this.anstime;
    }

    /**
     * @param date date
     */
    public void setanstime(Date date) {
        this.anstime = date;
    }

}
