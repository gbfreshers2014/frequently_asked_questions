/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author yavvan
 */
public class GiveAnswers {

    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    private Date date = new Date();
    private String userId, answer, question;
    private int questionId;
    /**
     * @return date
     */
    public Date getDate() {
        return date;
    }
    /**
     * @param date date
     */
    public void setDate(Date date) {
        this.date = date;
    }
    /**
     * @return questionId
     */
    public int getQuestionId() {
        return questionId;
    }
    /**
     * @param questionId questionId
     */
    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }
    /**
     * @return user
     */
    public String getUserId() {
        return userId;
    }
    /**
     * @param userId userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
    /**
     * @return answer
     */
    public String getAnswer() {
        return answer;
    }
    /**
     * @param answer answer
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }
    /**
     * @return question
     */
    public String getQuestion() {
        return question;
    }
    /**
     * @param question question
     */
    public void setQuestion(String question) {
        this.question = question;
    }
    /**
     * @return dateformat
     */
    public DateFormat getDateFormat() {
        return dateFormat;
    }
    /**
     * @param dateFormat dateformat
     */
    public void setDateFormat(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

}
