/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * @author yavvan
 */
public class GiveAnswerResponse extends AbstractResponse {
}
