/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.objects;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * @author yavvan
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAnswersResponse extends AbstractResponse {

    private List<AnswersDetails> report;

    /**
     * @return report
     */
    public List<AnswersDetails> getReport() {
        return this.report;
    }

    /**
     * @param list list
     */
    public void setReport(List<AnswersDetails> list) {
        this.report = list;

    }

}
