/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

/**
 * @author yavvan
 */
public interface AnswersDAO extends Transactional<AnswersDAO> {

    /**
     * @param userId user
     * @param question question
     * @param answer answer
     * @param date date
     */
    @SqlUpdate("insert into Answer (Ans_By, question_id, Answer,Ans_time) values (:userId,:question,:answer,:date) ")
    void insertanswers(@Bind("userId") String userId, @Bind("question") int question, @Bind("answer") String answer,
            @Bind("date") java.util.Date date);

    /**
     * close.
     */
    void close();
}
