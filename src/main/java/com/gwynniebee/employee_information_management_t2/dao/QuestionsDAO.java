/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

/**
 * @author yavvan
 */
public interface QuestionsDAO extends Transactional<QuestionsDAO> {

    /**
     * @param userId user
     * @param question question
     * @param date date
     * @return status
     */
    @SqlUpdate("insert into Questions (Que_By,question,Que_time)values(:userId,:question,:date)")
    int insertQuestion(@Bind("userId") String userId, @Bind("question") String question, @Bind("date") java.util.Date date);

    /**
     * close.
     */
    void close();
}
