/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.employee_information_management_t2.dao.mapper.AnswersMapper;
import com.gwynniebee.employee_information_management_t2.objects.AnswersDetails;

/**
 * @author yavvan
 */
public interface GetAnswersDAO extends Transactional<GetAnswersDAO> {
    /**
     * @param questionId questionId
     * @return answers
     */
    @RegisterMapper(AnswersMapper.class)
    @SqlQuery("select * from Answer where Question_id=:questionId")
    List<AnswersDetails> getAnswers(@Bind("questionId") int questionId);

    /**
     * close.
     */
    void close();

}
