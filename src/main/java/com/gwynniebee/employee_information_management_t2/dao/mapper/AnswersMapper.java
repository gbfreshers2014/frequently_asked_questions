/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.employee_information_management_t2.objects.AnswersDetails;

/**
 * @author yavvan
 */
public class AnswersMapper implements ResultSetMapper<AnswersDetails> {

    /*
     * (non-Javadoc)
     * @see org.skife.jdbi.v2.tweak.ResultSetMapper#map(int, java.sql.ResultSet,
     * org.skife.jdbi.v2.StatementContext)
     */
    @Override
    public AnswersDetails map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        AnswersDetails up = new AnswersDetails();
        up.setansBy(r.getString("Ans_By"));
        up.setanswer(r.getString("Answer"));
        up.setanstime(r.getDate("Ans_time"));
        up.setanswerID(r.getInt("answer_id"));
        return up;
    }
}
