/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.entities;

import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.dao.QuestionsDAO;
import com.gwynniebee.faqs_package.restlet.faqs_application;

/**
 * @author yavvan
 *
 */
public class QuestionEntityManager {

    private static final Logger LOG = LoggerFactory.getLogger(QuestionEntityManager.class);
    private static QuestionEntityManager manager = new QuestionEntityManager();
    private static DBI dbi = faqs_application.getDbi();

    /**
     * @return manager
     */
    public static QuestionEntityManager getInstance() {
        return manager;
    }

    /**
     * @param userId user
     * @param question question
     * @param date date
     * @return status
     */
    public int askquestion(String userId, String question, java.util.Date date) {
        LOG.info("QuestionEntityManager starts here");
        QuestionsDAO qDAO = dbi.open(QuestionsDAO.class);
        return qDAO.insertQuestion(userId, question, date);
    }
}
