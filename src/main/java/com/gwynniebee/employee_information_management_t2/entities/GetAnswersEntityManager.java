package com.gwynniebee.employee_information_management_t2.entities;

import java.util.List;

import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.dao.GetAnswersDAO;
import com.gwynniebee.employee_information_management_t2.objects.AnswersDetails;
import com.gwynniebee.faqs_package.restlet.faqs_application;

/**
 * @author yavvan
 *
 */
/**
 * Copyright 2012 GwynnieBee Inc.
 */
public class GetAnswersEntityManager {

    private static final Logger LOG = LoggerFactory.getLogger(GetAnswersEntityManager.class);
    private static GetAnswersEntityManager manager = new GetAnswersEntityManager();
    private static DBI dbi = faqs_application.getDbi();

    /**
     * @return instance of AddressEntityManager
     */
    public static GetAnswersEntityManager getInstance() {
        return manager;
    }

    /**
     * @param questionId questionId
     * @return details
     */
    public List<AnswersDetails> getAnswers(int questionId) {
        LOG.info("GetAnswersEntityManager starts here");
        GetAnswersDAO lDAO = dbi.open(GetAnswersDAO.class);
        List<AnswersDetails> ans = lDAO.getAnswers(questionId);
        LOG.info("GetAnswersEntityManager ends here");
        return ans;
    }
}
