/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.employee_information_management_t2.entities;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.dao.AnswersDAO;
import com.gwynniebee.faqs_package.restlet.faqs_application;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * @author yavvan
 */

public class AnswerEntityManager {

    private static final Logger LOG = LoggerFactory.getLogger(AnswerEntityManager.class);
    private static AnswerEntityManager manager = new AnswerEntityManager();

    /**
     * @return manager
     */
    public static AnswerEntityManager getInstance() {
        return manager;
    }

    /**
     * @param userId user
     * @param questionId question
     * @param answer answer
     * @param date date
     * @return responsestatus
     */
    public ResponseStatus giveans(String userId, int questionId, String answer, java.util.Date date) {
        Handle h = null;
        DBI dbi = faqs_application.getDbi();
        h = dbi.open();
        LOG.info("AnswerEntityManager starts here");
        AnswersDAO aDAO = h.attach(AnswersDAO.class);
        aDAO.insertanswers(userId, questionId, answer, date);
        ResponseStatus status = new ResponseStatus(0, "it worked ,yayyyy");
        h.close();
        LOG.info("AnswerEntityManager ends here");
        return status;
    }
}
