/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.faqs_package.restlet;

import java.util.Properties;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.restlet.Application;
import org.restlet.data.MediaType;
import org.restlet.routing.Router;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.faqs_package.restlet.resources.AskQuestionResource;
import com.gwynniebee.faqs_package.restlet.resources.GetAnswersResource;
import com.gwynniebee.faqs_package.restlet.resources.GiveAnswerResource;
import com.gwynniebee.iohelpers.IOGBUtils;
import com.gwynniebee.rest.service.restlet.GBRestletApplication;

/**
 * Barcode file upload service routing and controlling resources.
 * @author yavvan
 */

public class faqs_application extends GBRestletApplication {

    private static final Logger LOG = LoggerFactory.getLogger(faqs_application.class);
    private static Validator validator;
    private Properties serviceProperties;
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://induction-dev-blr.gwynniebee.com/gb_faqs";

    static final String USERNAME = "root";
    static final String PASSWORD = "";
    private static DBI dbi = new DBI(DB_URL, USERNAME, PASSWORD);

    /**
     * (From Application.getCurrent)<br>
     * This variable is stored internally as a thread local variable and updated
     * each time a call enters an application.<br>
     * <br>
     * Warning: this method should only be used under duress. You should by
     * default prefer obtaining the current application using methods such as
     * {@link org.restlet.resource.Resource#getApplication()} <br>
     * @return The current application.
     */
    public static faqs_application getCurrent() {
        return (faqs_application) Application.getCurrent();
    }

    /*
     * @return
     * @see org.restlet.Application#createInboundRoot()
     */
    @Override
    public synchronized Router createInboundRoot() {

        Router router = super.createInboundRoot();
        String resourceUrl = null;
        this.getMetadataService().setDefaultMediaType(MediaType.APPLICATION_JSON);

        resourceUrl = "/askquestion.json";
        router.attach(resourceUrl, AskQuestionResource.class);
        resourceUrl = "/giveanswer.json";
        router.attach(resourceUrl, GiveAnswerResource.class);
        resourceUrl = "/getanswers/{questionId}.json";
        router.attach(resourceUrl, GetAnswersResource.class);

        return router;
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#start()
     */
    @Override
    public synchronized void start() throws Exception {
        if (!this.isStarted()) {

            Class.forName(JDBC_DRIVER);
            dbi = new DBI(DB_URL, USERNAME, PASSWORD);
            // Prepare Properties
            this.serviceProperties = IOGBUtils.getPropertiesFromResource("/faqs_properties.properties");

            // Prepare validation factory
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            faqs_application.validator = factory.getValidator();
        }
        // below will make this.isStarted() true
        super.start();
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#stop()
     */
    @Override
    public synchronized void stop() throws Exception {
        if (!this.isStopped()) {
            LOG.info("Application.stop()");
        }
        // below will make this.isStopped() true
        super.stop();
    }

    /**
     * @return the serviceProps
     */
    public Properties getServiceProperties() {
        return this.serviceProperties;
    }

    /**
     * @return validator
     */
    public static Validator getValidator() {
        return validator;
    }
    
    /**
     * @return dbi
     */
    public static DBI getDbi() {
        if (dbi == null) {
            dbi = new DBI(DB_URL, USERNAME, PASSWORD);
        }
        return dbi;
    }
}
