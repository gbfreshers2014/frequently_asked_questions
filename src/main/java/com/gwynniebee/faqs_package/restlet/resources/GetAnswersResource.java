/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.faqs_package.restlet.resources;

import org.restlet.resource.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.entities.GetAnswersEntityManager;
import com.gwynniebee.employee_information_management_t2.objects.GetAnswersResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * @author yavvan
 */
public class GetAnswersResource extends AbstractServerResource {
    
    private static final Logger LOG = LoggerFactory.getLogger(AskQuestionResource.class);

    /**
     * @return answers
     */
    @Get
    public GetAnswersResponse getanswers() {
        LOG.info("u are at AskQuestionResource ");
        int questionId = Integer.parseInt(((String) this.getRequestAttributes().get("questionId")));
        GetAnswersResponse response = new GetAnswersResponse();
        ResponseStatus status = new ResponseStatus();
        response.setReport(GetAnswersEntityManager.getInstance().getAnswers(questionId));
        status.setCode(0);
        status.setMessage("Trying to return answers");
        response.setStatus(status);
        return response;
    }

}
