/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.faqs_package.restlet.resources;

import org.restlet.resource.Put;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.entities.QuestionEntityManager;
import com.gwynniebee.employee_information_management_t2.objects.GiveAnswerResponse;
import com.gwynniebee.employee_information_management_t2.objects.GiveAnswers;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * @author yavvan
 */
public class AskQuestionResource extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(AskQuestionResource.class);

    /**
     * @param a java object
     * @return GiveAnswerResponse
     */

    @Put
    public GiveAnswerResponse ask(GiveAnswers a) {

        LOG.info("u are at AskQuestionResource ");

        GiveAnswerResponse response = new GiveAnswerResponse();
        QuestionEntityManager.getInstance().askquestion(a.getUserId(), a.getQuestion(), a.getDate());

        LOG.info("u are the end of AskQuestionResource");

        ResponseStatus status = new ResponseStatus();
        status.setCode(0);
        status.setMessage("asking question");
        response.setStatus(status);

        return response;

    }
}
