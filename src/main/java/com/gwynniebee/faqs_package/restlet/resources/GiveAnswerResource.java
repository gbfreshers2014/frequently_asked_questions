/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.faqs_package.restlet.resources;

import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.employee_information_management_t2.entities.AnswerEntityManager;
import com.gwynniebee.employee_information_management_t2.objects.GiveAnswers;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * @author yavvan
 */
public class GiveAnswerResource extends AbstractServerResource {
    private static final Logger LOG = LoggerFactory.getLogger(GiveAnswerResource.class);

    /**
     * @param a object
     * @return status
     */
    @Post
    public ResponseStatus ask(GiveAnswers a) {

        LOG.info("u are at Giveanswer");
        return AnswerEntityManager.getInstance().giveans(a.getUserId(), a.getQuestionId(), a.getAnswer(), a.getDate());

    }
}
