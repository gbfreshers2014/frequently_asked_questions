/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.faqs_package.j2ee;

import com.gwynniebee.rest.service.j2ee.GBRestletServlet;
import com.gwynniebee.faqs_package.restlet.faqs_application;

/**
 * @author sarath
 */
@SuppressWarnings("serial")
public class RestfulServlet extends GBRestletServlet<faqs_application> {
    /**
     * This is called by tomcat's start of servlet.
     */
    public RestfulServlet() {
        this(new faqs_application());
    }

    /**
     * This is called by unit test which create a subclass of this class with subclass of application.
     * @param app application
     */
    public RestfulServlet(faqs_application app) {
        super(app);
    }
}
